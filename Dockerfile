# ---- Base Node ----
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libleveldb-dev libevent-dev libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ----
FROM base as build
RUN git clone https://github.com/CrypticApplications/MTR-Update.git /opt/mastertrader && \
    mkdir /opt/mastertrader/src/obj && \
    mkdir -p /opt/mastertrader/src/obj/zerocoin && \
    cd /opt/mastertrader/src/leveldb && \
    chmod +x build_detect_platform && \
    make libleveldb.a libmemenv.a && \
    cd /opt/mastertrader/src && \
    make -f makefile.unix

# ---- Release ----
FROM ubuntu:16.04 AS release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libboost-all-dev libleveldb-dev libevent-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r mastertrader && useradd -r -m -g mastertrader mastertrader
RUN mkdir /data
RUN chown mastertrader:mastertrader /data
COPY --from=build /opt/mastertrader/src/MasterTraderd /usr/local/bin/
USER mastertrader
VOLUME /data
EXPOSE 14474 14475
CMD ["/usr/local/bin/MasterTraderd", "-datadir=/data", "-conf=/data/MasterTrader.conf", "-server", "-txindex", "-printtoconsole"]